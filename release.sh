#!/bin/sh
set -e
releaseDir=$(mktemp -d)
[ -d ${CI_BUILDS_DIR}/forestsoft_de/gitlab-ci-template/template/ ] && srcDir=${CI_BUILDS_DIR}/forestsoft_de/gitlab-ci-template/template/ || srcDir="$(pwd)/template/"

cd $releaseDir

# Clone as mirror that we get tags
git clone git@github.com:Forestsoft-de/gitlab-ci-template.git
cd $releaseDir/gitlab-ci-template

LATEST_TAG=$(git ls-remote --tags git@github.com:Forestsoft-de/gitlab-ci-template.git | awk -F'/' '/[0-9].[0-9].[0-9].*/ { print $3}' | sort -nr | head -n1 | awk {'print $$2'}|sed 's/refs\/tags\///g')
echo "$CI_COMMIT_MESSAGE" | grep -q "minor change" && BUMP="minor" || BUMP="patch"
echo "$CI_COMMIT_MESSAGE" | grep -q "major change" && BUMP="major"

LATEST=$(docker run --rm marcelocorreia/semver semver -c -i $BUMP $LATEST_TAG)
echo "Tagging latest version: $LATEST"
echo "Replacing for release"
rsync -a --checksum  --delete $srcDir $releaseDir/gitlab-ci-template/dist/
find $releaseDir/gitlab-ci-template/dist -type f -print0 | xargs -0 sed -i "s,https://gitlab.com/forestsoft_de/gitlab-ci-template/-/raw/main/template,https://raw.githubusercontent.com/Forestsoft-de/gitlab-ci-template/$LATEST/dist,g"
git add .
git status
git commit -m "$CI_COMMIT_MESSAGE"
git tag $LATEST

echo "Commit main latest"
find $releaseDir/gitlab-ci-template/dist -type f -print0 | xargs -0 sed -i "s,https://raw.githubusercontent.com/Forestsoft-de/gitlab-ci-template/$LATEST/dist,https://raw.githubusercontent.com/Forestsoft-de/gitlab-ci-template/main/dist,g"
git add .
git commit -m "$CI_COMMIT_MESSAGE"

git push
git push --tags


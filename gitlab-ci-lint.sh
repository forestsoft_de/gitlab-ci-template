#!/bin/bash
set -e

if [[ $(ls $1) ]]; then
    INPUT=$(cat $1)
else
    INPUT=$(</dev/stdin)
fi

if [ "x$CI_API_V4_URL" != "x" ] ; then
    # if we're running as a CI job, pick the provided API base url unless
    # overridden
    GITLAB_API_URL="${GITLAB_API_URL:-$CI_API_V4_URL}"
else
    # otherwise, use gitlab.com's API URL unless overridden
    GITLAB_API_URL="${GITLAB_API_URL:-https://gitlab.com/api/v4}"
fi

[ "$INPUT" == "" ] && echo "YML file is not readable" && exit 1

CONTENT=$(echo "$INPUT" | python3 -c 'import json,sys; print(json.dumps(sys.stdin.read()))')

export LINT_JOB_TOKEN=${LINT_JOB_TOKEN:-"$(cat CI_JOB_TOKEN.txt)"}
RESPONSE=$(curl -s --header "Content-Type: application/json" --header "PRIVATE-TOKEN: $LINT_JOB_TOKEN" $GITLAB_API_URL/ci/lint --data @- <<CURL_DATA
{"content": $CONTENT}
CURL_DATA
)
STATUS=$(echo $RESPONSE | jq -r '.status')

if [[ $STATUS == 'valid' ]]; then
    echo "configuration is valid"
    exit 0
elif [[ $STATUS == 'invalid' ]]; then
    echo "configuration is invalid"
    echo $RESPONSE | jq '.errors'
    exit -1
else
    echo "configuration is invalid, unknown status '$STATUS'"
    exit -2
fi